package ExcelReadWritePCKG;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import org.openqa.selenium.support.ui.Select;


import jxl.*;
import jxl.read.biff.BiffException;

public class DataDriven_Framework {
        public static void main(String[] args) throws InterruptedException {
               WebDriver driver;
               System.setProperty("webdriver.chrome.driver", "C:\\Users\\Integrated\\OneDrive\\Desktop\\Selenium Driver\\chromedriver.exe");	
       	    	driver = new ChromeDriver();
       		
       		
       	    	driver.manage().window().maximize();
               //Login
              driver.navigate().to("http://demo.guru99.com/test/newtours/");
              // driver.navigate().to("https://askzuma.com/");
              // driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
               Thread.sleep(5000);
               
               try {
                     Workbook workbook = Workbook.getWorkbook(new File("C:\\Users\\Integrated\\OneDrive\\Desktop\\ExcelReadWrite\\ExcelReadWrite\\src\\ExcelReadWritePCKG\\Excel1.xls"));
                    // Sheet sheet = workbook.getSheet(1);
                     Sheet sheet = workbook.getSheet("Sheet1");
                     driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/form/table/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/input")).sendKeys(sheet.getCell(0, 1).getContents());
                    // driver.findElement(By.name("userName")).sendKeys("demo");

                     driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/form/table/tbody/tr[4]/td/table/tbody/tr[3]/td[2]/input")).sendKeys(sheet.getCell(1, 1).getContents());
                     // driver.findElement(By.name("password")).sendKeys("demo");

                     driver.findElement(By.name("submit")).click();
                     Thread.sleep(5000);
                     driver.findElement(By.linkText("Flights")).click();
                     Select Passangers= new Select(driver.findElement(By.cssSelector("select[name='passCount']")));
                     Passangers.selectByVisibleText(sheet.getCell(2, 1).getContents());
                     Select Departingfrom = new Select(driver.findElement(By.cssSelector("select[name='fromPort']")));
                      Departingfrom.selectByVisibleText(sheet.getCell(3, 1).getContents());
                      Select FromMonth = new Select(driver.findElement(By.cssSelector("select[name='fromMonth']")));
                      FromMonth.selectByValue((sheet.getCell(4, 1).getContents()));
                      Select ArrivingIn = new Select(driver.findElement(By.cssSelector("select[name='toPort']")));
                      ArrivingIn.selectByVisibleText(sheet.getCell(5,1).getContents());
                      Select ToMonth = new Select(driver.findElement(By.cssSelector("select[name='toMonth']")));
                      ToMonth.selectByValue(sheet.getCell(6,1).getContents());
                      driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[9]/td[2]/font/font/input")).click();
                      driver.findElement(By.name("findFlights")).click();
                    //  driver.findElement(By.name("reserveFlights")).click();
                     // driver.findElement(By.name("passFirst0")).sendKeys(sheet.getCell(7, 1).getContents());
                     // driver.findElement(By.name("passLast0")).sendKeys(sheet.getCell(8, 1).getContents());
                     // driver.findElement(By.name("creditnumber")).sendKeys(sheet.getCell(9, 1).getContents());
                     // driver.findElement(By.name("buyFlights")).click();           
              } catch (BiffException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
              } catch (IOException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
              }
             //Close Browser
              // driver.quit();
               Thread.sleep(5000);
               driver.close();
        }
        }