package ExcelReadWritePCKG;

import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import org.testng.annotations.BeforeTest;

import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class Loop_DataEntry_Login_csv {
	String CSV_Path;
	WebDriver driver;
 
  @BeforeTest
  public void beforeTest() throws InterruptedException {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\Integrated\\OneDrive\\Desktop\\Selenium Driver\\chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.get("http://demo.guru99.com/v4/index.php");
	  CSV_Path="C:\\Users\\Integrated\\OneDrive\\Desktop\\ExcelReadWrite\\ExcelReadWrite\\src\\ExcelReadWritePCKG\\Guru_99_Loop.csv";
	  Thread.sleep(3000);
	  
  }

  @Test(priority =0)
  public void enterValues() throws CsvValidationException, IOException, InterruptedException {
	  CSVReader reader = new CSVReader(new FileReader(CSV_Path));
	  String [] csvCell;
	  while((csvCell = reader.readNext()) != null)
	  {
		  String UserName = csvCell[0];
		  String Password = csvCell[1];
		  driver.findElement(By.xpath("/html/body/form/table/tbody/tr[1]/td[2]/input")).sendKeys(UserName);
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("/html/body/form/table/tbody/tr[2]/td[2]/input")).sendKeys(Password);
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/input[1]")).click();
		  Thread.sleep(3000);
		  Alert alert = driver.switchTo().alert();
		  alert.accept();
		  Thread.sleep(3000);		  
	  }  
  }
  
  @AfterTest
  public void afterTest() throws InterruptedException {
	  Thread.sleep(3000);
	  driver.close();
  }

}
