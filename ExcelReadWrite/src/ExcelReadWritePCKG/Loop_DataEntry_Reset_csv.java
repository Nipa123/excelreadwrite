package ExcelReadWritePCKG;

//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

public class Loop_DataEntry_Reset_csv {

	public static void main(String[] args) throws CsvValidationException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		String CSV_Path;
		
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Integrated\\OneDrive\\Desktop\\Selenium Driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/v4/index.php");
		CSV_Path = "C:\\Users\\Integrated\\OneDrive\\Desktop\\ExcelReadWrite\\ExcelReadWrite\\src\\ExcelReadWritePCKG\\Guru_99_Loop.csv";
		CSVReader reader = new CSVReader(new FileReader(CSV_Path));
		String [] csvCell;
		Thread.sleep(3000);
		while ((csvCell = reader.readNext()) != null)
		{
			String name = csvCell[0];
			String password = csvCell[1];
			driver.findElement(By.xpath("/html/body/form/table/tbody/tr[1]/td[2]/input")).sendKeys(name);
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/form/table/tbody/tr[2]/td[2]/input")).sendKeys(password);
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/input[2]")).click();
			Thread.sleep(3000);
		}
		driver.close();
	}

}
