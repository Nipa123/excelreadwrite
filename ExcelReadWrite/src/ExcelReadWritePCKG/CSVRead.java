package ExcelReadWritePCKG;

import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

public class CSVRead {

	//Provide CSV file path. It Is In D: Drive.
	String CSV_PATH;
	WebDriver driver;

	@BeforeTest
	public void setup() throws Exception {

		String path = System.getProperty("user.dir");
		System.out.print("Where am I? "+path);
		//CSV_PATH=path+"/data/Detail.csv";
		CSV_PATH="C:\\Users\\Integrated\\OneDrive\\Desktop\\ExcelReadWrite\\ExcelReadWrite\\src\\ExcelReadWritePCKG\\Detail.csv";
//          		C:\Users\Integrated\OneDrive\Desktop\ExcelReadWrite\ExcelReadWrite\src\ExcelReadWritePCKG\Detail.csv	
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Integrated\\OneDrive\\Desktop\\Selenium Driver\\chromedriver.exe");	
	    driver = new ChromeDriver();
		
		
	    driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://demo.guru99.com/test/newtours/register.php");
		
		
	}

	@Test
	public void csvDataRead() throws IOException, CsvValidationException{

		CSVReader reader = new CSVReader(new FileReader(CSV_PATH));
		String [] csvCell;
		//while loop will be executed till the last line In CSV.
		while ((csvCell = reader.readNext()) != null) {   
			String FName = csvCell[0];
			String LName = csvCell[1];
			String Email = csvCell[2];
			String Mob = csvCell[3];
			String city = csvCell[4];
			// driver.findElement(By.xpath("//input[@name='FirstName']")).sendKeys("fname1");
			driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/input")).sendKeys(FName);
			// driver.findElement(By.xpath("//input[@name='FirstName']")).sendKeys("lname1");
			driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[3]/td[2]/input")).sendKeys(LName);
			driver.findElement(By.xpath("//*[@id=\"userName\"]")).sendKeys(Email);
			driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[4]/td[2]/input")).sendKeys(Mob);
			driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[8]/td[2]/input")).sendKeys(city);
			driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[17]/td/input")).click();
			driver.get("http://demo.guru99.com/test/newtours/register.php");
		}  
	}
}
